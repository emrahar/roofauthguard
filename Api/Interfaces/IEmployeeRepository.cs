using System.Collections.Generic;
using Api.Entities;

namespace Api.Interfaces;

public interface IEmployeeRepository
{
    public void Add(Employee employee);

    public List<Employee> All();
}