using System;
using System.Collections.Generic;
using Api.Entities;
using Api.Interfaces;
using Api.Repository;

namespace Api.Services;

public class EmployeeService : IEmployeeService
{

    private IEmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository)
    {
        this.employeeRepository = employeeRepository;
    }
    
    public void AddEmployee(Employee employee)
    {
        
        ValidateEmployee(employee);
        employeeRepository.Add(employee);
        
   
    }
    
    private void ValidateEmployee(Employee employee)
    {
        if (String.IsNullOrEmpty(employee.Name))
        {
            throw new Exception("NULL_EMPLOYEE_NAME");
        }
        if (String.IsNullOrEmpty(employee.Lastname))
        {
            throw new Exception("NULL_EMPLOYEE_LASTNAME");
        }
    }
    
    public List<Employee> GetAllEmployees()
    {

        return employeeRepository.All();
    }

}