using System.ComponentModel.DataAnnotations;

namespace Api.Entities;

public class Employee
{
    [Key]
    public string Name { get; set; }
    public string Lastname { get; set; }
    public int Age { get; set; }

    public Employee(string name, string lastname, int age)
    {
        Name = name;
        Lastname = lastname;
        Age = age;
    }
}