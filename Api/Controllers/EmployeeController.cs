﻿using Api.Entities;
using Api.Interfaces;
using Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{

    [Route("employee")]
    [Produces("application/json")]
    public class IdentityController : ControllerBase
    {

        private IEmployeeService employeeService;

        public IdentityController(EmployeeService employeeService)
        {
            this.employeeService = employeeService;
        }

        [HttpPost]
        [Route("create")]
        public IActionResult Post([FromBody] Employee employee)
        {
            employeeService.AddEmployee(employee);
           
            
            return new OkObjectResult(employee);
        }
        
        [Route("list")]
        public IActionResult Get()
        {
            return new OkObjectResult(employeeService.GetAllEmployees());
        }
    }
}