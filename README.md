# Roof Auth Guard Case

## Project Structure

Three projects have been developed for the case

* Api 
    * Responsible for the management of employee based crud operations. 
* AuthServer (Auth Guard)
    * Responsible for Oauth/OpenID based token flow
* UnitTestProject
    * Simple Test Implementations
  
## Testing
RoofAuthGuard_Insomnia_ProjectExport.json can be found under Api project.
[Download Insomnia](https://insomnia.rest/download)
