using System.Collections.Generic;
using Api.Entities;

namespace Api.Interfaces;

public interface IEmployeeService
{
    public void AddEmployee(Employee employee);

    public List<Employee> GetAllEmployees();
}