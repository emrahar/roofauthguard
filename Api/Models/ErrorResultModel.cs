using System;

namespace Api.Models;

public class ErrorResultModel
{
    public string Type { get; set; }
    public string Message { get; set; }
    public string StackTrace { get; set; }

    public ErrorResultModel(Exception ex)
    {
        Type = ex.GetType().Name;
        Message = ex.Message;
        StackTrace = ex.ToString();
    }
}