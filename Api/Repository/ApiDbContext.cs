using Api.Entities;
using Microsoft.EntityFrameworkCore;

namespace Api.Repository;

public class ApiDbContext : DbContext
{
    
    public ApiDbContext(DbContextOptions<ApiDbContext> options)
        : base(options)
    {
    }

    public DbSet<Employee> Employees { get; set; }
    
}