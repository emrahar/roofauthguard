using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Xunit;

namespace UnitTestProject;

public class UnitTest
{
    [Fact]
    public async void ApiAuthenticationTest()
    {
        var client = new TestProvider().AuthClient;
        
        HttpContent content = new FormUrlEncodedContent(new[]
        {
            new KeyValuePair<string, string>("client_id", "client"),
            new KeyValuePair<string, string>("client_secret", "secret"),
            new KeyValuePair<string, string>("scope", "api1"),
            new KeyValuePair<string, string>("grant_type", "client_credentials")
        });
        var okResult = await client.PostAsync("connect/token", content);
        
        okResult.EnsureSuccessStatusCode();
        
        Assert.Equal(HttpStatusCode.OK, okResult.StatusCode);
    }
    
    [Fact]
    public async void GetEmployees_FailedAuthentication_Test()
    {
        var client = new TestProvider().ApiClient;
        
        var okResult = await client.GetAsync("employee/list");
        
        Assert.Equal(HttpStatusCode.Unauthorized, okResult.StatusCode);
    }
}