using System.Collections.Generic;
using System.Collections.Immutable;
using Api.Entities;
using Api.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Api.Repository;

public class EmployeeRepository : IEmployeeRepository
{
    
    private readonly ApiDbContext _context;
    
    public EmployeeRepository(ApiDbContext context)
    {
        _context = context;
    }
    
    public void Add(Employee employee)
    {
        _context.Employees.Add(employee);
        _context.SaveChanges();
    }
    
    public List<Employee> All()
    {
        return _context.Employees.ToListAsync().Result;
    }

}