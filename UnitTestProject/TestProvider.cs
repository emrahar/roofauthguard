using System.Net.Http;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;

namespace UnitTestProject;

public class TestProvider
{

    public HttpClient AuthClient { get; set; }
    public HttpClient ApiClient { get; set; }

    public TestProvider()
    {
        var authServer = new TestServer(new WebHostBuilder().UseStartup<IdentityServer.Startup>());
        AuthClient = authServer.CreateClient();
        
        var apiServer = new TestServer(new WebHostBuilder().UseStartup<Api.Startup>());
        ApiClient = apiServer.CreateClient();
    }

}